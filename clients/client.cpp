// Copyright (C) 2017, All rights reserved.
// Author: bysin@bysin.net (Benjamin Kittridge)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <math.h>
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread/future.hpp>
#include <experimental/string_view>
#include <memory>
#include <mutex>
#include <numeric>
#include <string>
#include <thread>
#include <vector>

using std::experimental::string_view;
using boost::asio::ip::tcp;

namespace {

const bool kDebug = false;
const std::string kHeaderToken = "\r\n\r\n";
const std::string kRequestData =
    "GET / HTTP/1.1\r\nConnection: keep-alive\r\nHost: localhost\r\n\r\n";
const uint64_t kSecondsPerStage = 15;
const uint64_t kMaxClientCount = 30000;

class Log {
 public:
  ~Log() {
    if (kDebug) {
      stream_ << '\n';
      std::cerr << stream_.str();
    }
  }

  template <typename T>
  std::ostream &operator<<(T &&t) {
    return stream_ << std::forward<T>(t);
  }

 private:
  std::ostringstream stream_;
};

std::vector<string_view> Split(string_view data, string_view delim) {
  std::vector<string_view> items;
  while (!data.empty()) {
    const size_t match = data.find(delim);
    const bool eof = (match == string_view::npos);
    const string_view part(data.data(), eof ? data.size() : match);
    items.emplace_back(part);
    if (eof)
      break;
    data.remove_prefix(match + delim.size());
  }
  return items;
}

class Stats {
 public:
  Stats();

  void Record(double latency);
  void RecordError();

  void SetConnectionState(uint64_t client_id, bool connected);

  void Report(uint64_t client_count);

 private:
  static double Percentile(const std::vector<double> &points,
                           double percentile);

  std::mutex mutex_;
  std::vector<double> timepoints_;
  uint64_t error_count_;
  std::chrono::time_point<std::chrono::high_resolution_clock> last_report_time_;
  std::map<uint64_t, bool> connected_;
};

Stats::Stats()
    : error_count_(0),
      last_report_time_(std::chrono::high_resolution_clock::now()) {
}

void Stats::Record(double latency) {
  std::unique_lock<std::mutex> l(mutex_);
  timepoints_.emplace_back(latency);
}

void Stats::RecordError() {
  std::unique_lock<std::mutex> l(mutex_);
  error_count_++;
}

void Stats::Report(uint64_t client_count) {
  std::vector<double> timepoints;
  uint64_t error_count;
  uint64_t connected_clients = 0;
  {
    std::unique_lock<std::mutex> l(mutex_);
    timepoints = std::move(timepoints_);
    timepoints_.clear();
    error_count = error_count_;
    error_count_ = 0;
    for (const auto &client : connected_) {
      if (client.second)
        connected_clients++;
    }
  }
  const std::chrono::time_point<std::chrono::high_resolution_clock> now =
      std::chrono::high_resolution_clock::now();
  const std::chrono::duration<double> duration = now - last_report_time_;
  last_report_time_ = now;

  std::sort(timepoints.begin(), timepoints.end());
  const double mean_latency =
      timepoints.empty()
          ? 0
          : (std::accumulate(timepoints.begin(), timepoints.end(), 0.) /
             timepoints.size());
  std::cout << "ts=" << time(nullptr) << " "
            << "conn_clients=" << connected_clients << " "
            << "total_clients=" << client_count << " "
            << "qps=" << (timepoints.size() / duration.count()) << " "
            << "errors=" << (error_count / duration.count()) << " "
            << "mean=" << mean_latency << " "
            << "50ile=" << Percentile(timepoints, 0.50) << " "
            << "90ile=" << Percentile(timepoints, 0.90) << " "
            << "99ile=" << Percentile(timepoints, 0.99) << std::endl;
}

void Stats::SetConnectionState(uint64_t client_id, bool connected) {
  std::unique_lock<std::mutex> l(mutex_);
  connected_[client_id] = connected;
}

// static
double Stats::Percentile(const std::vector<double> &points, double percentile) {
  if (points.empty())
    return 0.;
  const double n = (points.size() - 1) * percentile + 1;
  if (n == 1.) {
    return points.front();
  } else if (n == points.size()) {
    return points.back();
  } else {
    const size_t k = static_cast<size_t>(n);
    const double d = n - k;
    return points[k - 1] + d * (points[k] - points[k - 1]);
  }
}

class Client : public std::enable_shared_from_this<Client> {
 public:
  typedef std::shared_ptr<Client> Handle;

  Client(Stats *stats, boost::asio::io_service *io_service, uint64_t client_id,
         const std::string &address, uint16_t port);

  void Start();

 private:
  void Connect();
  void ConnectDone(const boost::system::error_code &error);

  void Write();
  void WriteDone(const boost::system::error_code &error,
                 size_t bytes_transferred);

  void ReadHeader();
  void ReadHeaderDone(const boost::system::error_code &error,
                      size_t bytes_transferred);

  void Dispatch(const std::string &header);
  void ReadDataDone(const boost::system::error_code &error,
                    size_t bytes_transferred);

  void Error(const std::string &error, const std::string &message = "");
  void Finalize(bool success);

  Stats *stats_;
  const uint64_t client_id_;
  const std::string address_;
  const uint16_t port_;
  tcp::socket socket_;
  std::chrono::time_point<std::chrono::high_resolution_clock> start_time_;
  boost::asio::streambuf read_buffer_;
};

Client::Client(Stats *stats, boost::asio::io_service *io_service,
               uint64_t client_id, const std::string &address, uint16_t port)
    : stats_(stats),
      client_id_(client_id),
      address_(address),
      port_(port),
      socket_(*io_service) {
}

void Client::Start() {
  Connect();
}

void Client::Error(const std::string &error, const std::string &message) {
  Log() << "Client error: " << error << ": " << message;

  boost::system::error_code ec;
  socket_.close(ec);

  stats_->RecordError();
  Finalize(false);
}

void Client::Connect() {
  boost::asio::ip::tcp::endpoint endpoint(
      boost::asio::ip::address::from_string(address_), port_);
  socket_.async_connect(endpoint,
                        std::bind(&Client::ConnectDone, shared_from_this(),
                                  std::placeholders::_1));
}

void Client::ConnectDone(const boost::system::error_code &error) {
  if (error)
    return Error("Connection failed", error.message());

  stats_->SetConnectionState(client_id_, true);

  socket_.set_option(boost::asio::ip::tcp::no_delay(true));
  socket_.set_option(boost::asio::socket_base::linger(false, 0));

  Write();
}

void Client::Write() {
  start_time_ = std::chrono::high_resolution_clock::now();
  boost::asio::async_write(
      socket_, boost::asio::buffer(kRequestData.data(), kRequestData.size()),
      boost::asio::transfer_all(),
      std::bind(&Client::WriteDone, shared_from_this(), std::placeholders::_1,
                std::placeholders::_2));
}

void Client::WriteDone(const boost::system::error_code &error,
                       size_t bytes_transferred) {
  if (error)
    return Error("Write failed", error.message());
  ReadHeader();
}

void Client::ReadHeader() {
  typedef typename boost::asio::streambuf::const_buffers_type
      const_buffers_type;
  typedef boost::asio::buffers_iterator<const_buffers_type> iterator;
  const_buffers_type buffers = read_buffer_.data();
  iterator begin = iterator::begin(buffers);
  iterator end = iterator::end(buffers);
  iterator result =
      std::search(begin, end, kHeaderToken.begin(), kHeaderToken.end());
  if (result != end) {
    const size_t header_length =
        static_cast<size_t>(std::distance(begin, result));
    const std::string header =
        std::string(boost::asio::buffers_begin(read_buffer_.data()),
                    boost::asio::buffers_begin(read_buffer_.data()) +
                        static_cast<std::streamsize>(header_length));
    read_buffer_.consume(header_length + kHeaderToken.size());
    return Dispatch(header);
  }
  boost::asio::async_read(
      socket_, read_buffer_, boost::asio::transfer_at_least(1),
      std::bind(&Client::ReadHeaderDone, shared_from_this(),
                std::placeholders::_1, std::placeholders::_2));
}

void Client::ReadHeaderDone(const boost::system::error_code &error,
                            size_t bytes_transferred) {
  if (error)
    return Error("Read failed", error.message());
  ReadHeader();
}

void Client::Dispatch(const std::string &header) {
  const auto lines = Split(header, "\r\n");
  if (lines.size() < 1)
    return Error("Invalid response");

  bool first = true;
  size_t content_length = 0;
  for (const auto line : lines) {
    if (first) {
      const std::vector<string_view> first_parts = Split(line, " ");
      if (first_parts.size() < 2)
        return Error("Invalid header");
      if (first_parts[1] != "200")
        return Error("Error returned from server");
      first = false;
    } else {
      const size_t pos = line.find(":");
      if (pos == std::string::npos)
        continue;

      std::string key = line.substr(0, pos).to_string();
      std::string value = line.substr(pos + 1).to_string();
      boost::algorithm::trim(key);
      boost::algorithm::trim(value);
      if (boost::algorithm::iequals(key, "content-length")) {
        content_length = boost::lexical_cast<size_t>(value);
      }
    }
  }

  if (content_length == 0) {
    Finalize(true);
  } else {
    if (read_buffer_.size() < content_length) {
      boost::asio::async_read(
          socket_, read_buffer_,
          boost::asio::transfer_at_least(content_length - read_buffer_.size()),
          std::bind(&Client::ReadDataDone, shared_from_this(),
                    std::placeholders::_1, std::placeholders::_2));
    } else {
      ReadDataDone(boost::system::error_code(), content_length);
    }
  }
}

void Client::ReadDataDone(const boost::system::error_code &error,
                          size_t bytes_transferred) {
  if (error)
    return Error("Read failed", error.message());
  read_buffer_.consume(bytes_transferred);
  Finalize(true);
}

void Client::Finalize(bool success) {
  if (success) {
    const std::chrono::duration<double> duration =
        std::chrono::high_resolution_clock::now() - start_time_;
    stats_->Record(duration.count());
  }
  if (socket_.is_open()) {
    Write();
  } else {
    stats_->SetConnectionState(client_id_, false);
    Connect();
  }
}

}  // anonymous namespace

int main(int argc, char **argv) {
  if (argc <= 2) {
    std::cerr << argv[0] << " <address> <port>" << std::endl;
    return -1;
  }

  const char *address = argv[1];
  const uint16_t port = boost::lexical_cast<uint16_t>(argv[2]);

  Stats stats;
  boost::asio::io_service io_service;
  boost::asio::io_service::work work(io_service);

  const uint64_t hardware_concurrency = std::thread::hardware_concurrency();
  assert(hardware_concurrency != 0);
  std::vector<std::unique_ptr<std::thread>> threads;
  for (uint64_t i = 0; i < hardware_concurrency; i++) {
    threads.emplace_back(
        std::make_unique<std::thread>([&io_service]() { io_service.run(); }));
  }

  std::vector<Client::Handle> clients;
  uint64_t client_count = 0;
  uint64_t next_stage = 0;
  for (;;) {
    while (clients.size() < client_count) {
      auto client = std::make_shared<Client>(&stats, &io_service,
                                             clients.size(), address, port);
      client->Start();
      clients.emplace_back(std::move(client));
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));
    stats.Report(clients.size());
    if (++next_stage >= kSecondsPerStage) {
      client_count =
          std::max(1ul, static_cast<uint64_t>(
                            ceil(client_count + (client_count * log(1.2)))));
      if (client_count > kMaxClientCount)
        break;
      next_stage = 0;
    }
  }
  return 0;
}

