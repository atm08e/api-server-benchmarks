var hello_world = "Hello World";
var http = require("http");
http.createServer(function (request, response) {
   response.writeHead(200, {'Content-Length': hello_world.length,
                            'Connection': 'keep-alive'});
   response.end(hello_world);
}).listen(8888);

