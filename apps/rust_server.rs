#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;

use rocket::LoggingLevel;
use rocket::config::{Config, Environment};

#[get("/")]
fn hello() -> &'static str {
    "Hello world"
}

fn main() {
	let config = Config::build(Environment::Staging)
		.address("0.0.0.0")
		.port(8888)
		.workers(8000)
        .log_level(LoggingLevel::Critical)
		.unwrap();
    rocket::custom(config, false).mount("/", routes![hello]).launch();
}
