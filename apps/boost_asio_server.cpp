#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <cassert>
#include <condition_variable>
#include <cstdint>
#include <experimental/string_view>
#include <functional>
#include <iostream>
#include <mutex>
#include <queue>

using std::experimental::string_view;
using boost::asio::ip::tcp;

namespace {

const bool kDebug = false;
const double kIdleThresholdSecs = 15.0;
const uint64_t kMinThreads = 4;
const uint64_t kThreadStackSize = 4096;
const std::string kHeaderToken = "\r\n\r\n";
const std::string kResponse = "Hello World";

class Log {
 public:
  ~Log() {
    if (kDebug) {
      stream_ << '\n';
      std::cerr << stream_.str();
    }
  }

  template <typename T>
  std::ostream &operator<<(T &&t) {
    return stream_ << std::forward<T>(t);
  }

 private:
  std::ostringstream stream_;
};

class DynamicThreadPool {
 public:
  typedef std::function<void()> CallbackHandler;

  explicit DynamicThreadPool(uint64_t min_threads = kMinThreads)
      : min_threads_(min_threads),
        thread_count_(0),
        work_count_(0),
        stopped_(false) {
    StartThreads();
  }

  ~DynamicThreadPool() { StopThreads(); }

  void Enqueue(CallbackHandler handler) {
    {
      std::unique_lock<std::mutex> lock(lock_);
      assert(stopped_ == false);
      queue_.emplace(std::move(handler));
      work_count_++;
      StartThreads();
    }
    cv_.notify_one();
  }

 private:
  void StartThreads() {
    const uint64_t count = std::max(min_threads_, work_count_);
    for (uint64_t i = thread_count_; i < count; ++i) {
      thread_count_++;
      boost::thread::attributes attrs;
      attrs.set_stack_size(kThreadStackSize);
      threads_.add_thread(new boost::thread(
          attrs, std::bind(&DynamicThreadPool::ThreadMain, this)));
    }
  }

  void StopThreads() {
    {
      std::unique_lock<std::mutex> lock(lock_);
      if (stopped_)
        return;
      stopped_ = true;
    }
    cv_.notify_all();
    threads_.join_all();
  }

  void ThreadMain() {
    for (;;) {
      {
        std::unique_lock<std::mutex> lock(lock_);
        const bool signaled = cv_.wait_for(
            lock, std::chrono::duration<double>(kIdleThresholdSecs),
            [&]() { return !queue_.empty() || stopped_; });
        if ((!signaled && thread_count_ > min_threads_) ||
            (queue_.empty() && stopped_)) {
          thread_count_--;
          break;
        }
        if (queue_.empty()) {
          continue;
        }
        CallbackHandler task = std::move(queue_.front());
        queue_.pop();
        lock.unlock();

        try {
          task();
        } catch (...) {
          Log() << "Handler threw uncaught exception";
        }
      }

      std::unique_lock<std::mutex> mutex(lock_);
      work_count_--;
    }
  }

  boost::thread_group threads_;
  std::condition_variable_any cv_;
  std::mutex lock_;
  std::queue<CallbackHandler> queue_;
  uint64_t min_threads_;
  uint64_t thread_count_;
  uint64_t work_count_;
  bool stopped_;
};

std::vector<string_view> Split(string_view data, string_view delim) {
  std::vector<string_view> items;
  while (!data.empty()) {
    const size_t match = data.find(delim);
    const bool eof = (match == string_view::npos);
    const string_view part(data.data(), eof ? data.size() : match);
    items.emplace_back(part);
    if (eof)
      break;
    data.remove_prefix(match + delim.size());
  }
  return items;
}

class Peer : public std::enable_shared_from_this<Peer> {
 public:
  explicit Peer(boost::asio::io_service *io_service) : socket_(*io_service) {}

  void Start();

  tcp::socket &socket() { return socket_; }

 private:
  bool Dispatch();
  bool Respond(int32_t code);

  tcp::socket socket_;
  boost::asio::streambuf read_buffer_;
};

void Peer::Start() {
  socket_.set_option(boost::asio::ip::tcp::no_delay(true));
  socket_.set_option(boost::asio::socket_base::linger(false, 0));
  while (Dispatch()) {
  }
}

bool Peer::Dispatch() {
  std::string header;
  for (;;) {
    typedef typename boost::asio::streambuf::const_buffers_type
        const_buffers_type;
    typedef boost::asio::buffers_iterator<const_buffers_type> iterator;
    const_buffers_type buffers = read_buffer_.data();
    iterator begin = iterator::begin(buffers);
    iterator end = iterator::end(buffers);
    iterator result =
        std::search(begin, end, kHeaderToken.begin(), kHeaderToken.end());
    if (result != end) {
      const size_t header_length =
          static_cast<size_t>(std::distance(begin, result));
      header = std::string(boost::asio::buffers_begin(read_buffer_.data()),
                           boost::asio::buffers_begin(read_buffer_.data()) +
                               static_cast<std::streamsize>(header_length));
      read_buffer_.consume(header_length + kHeaderToken.size());
      break;
    }
    boost::system::error_code ec;
    boost::asio::read(socket_, read_buffer_, boost::asio::transfer_at_least(1),
                      ec);
    if (ec) {
      if (ec != boost::asio::error::eof)
        Log() << "Read error: " << ec.message();
      return false;
    }
  }

  const auto lines = Split(header, "\r\n");
  if (lines.size() < 1)
    return Respond(400);
  const std::vector<string_view> first_parts = Split(lines[0], " ");
  if (first_parts.size() != 3)
    return Respond(400);
  if (first_parts[0] != "GET" || first_parts[1] != "/")
    return Respond(405);
  return Respond(200);
}

bool Peer::Respond(int32_t code) {
  std::ostringstream ss;
  ss << "HTTP/1.1 " << code << "\r\n";
  ss << "Server: Performance Test\r\n";
  ss << "Connection: keep-alive\r\n";
  if (code == 200) {
    ss << "Content-Length: " << kResponse.size() << "\r\n";
    ss << "\r\n";
    ss << kResponse;
  } else {
    ss << "Content-Length: 0\r\n";
    ss << "\r\n";
  }
  const std::string response = ss.str();

  boost::system::error_code ec;
  boost::asio::write(socket_,
                     boost::asio::buffer(response.data(), response.size()),
                     boost::asio::transfer_all(), ec);
  if (ec) {
    Log() << "Write error: " << ec.message();
    return false;
  }
  return true;
}

}  // anonymous namespace

int main(int argc, char **argv) {
  if (argc <= 1) {
    std::cerr << argv[0] << " <port>" << std::endl;
    return -1;
  }

  boost::asio::io_service io_service;
  tcp::endpoint endpoint(tcp::v4(), boost::lexical_cast<uint16_t>(argv[1]));
  tcp::acceptor acceptor(io_service, endpoint);
  DynamicThreadPool thread_pool;
  for (;;) {
    auto peer = std::make_shared<Peer>(&io_service);
    acceptor.accept(peer->socket());
    thread_pool.Enqueue(std::bind(&Peer::Start, std::move(peer)));
  }
  return 0;
}

